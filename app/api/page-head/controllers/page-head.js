'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/3.0.0-beta.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

const { sanitizeEntity } = require('strapi-utils');

module.exports = {
    async find(ctx) {
        let entities;
        
        if(ctx.query.application_id&&ctx.query.select&&ctx.query.url){
            entities = await strapi.query('page-head').model.find({
                applications:{
                    $in:[ctx.query.application_id]
                },
                canonical:{
                    $in:[ctx.query.url,"/"]
                }
            }).select(ctx.query.select);
        }else{
            entities = await strapi.query('page-head').find(ctx.query)
        }
        
        return entities.map(entity => sanitizeEntity(entity, { model: strapi.models['page-head'] })).map(e=>{
            return {
                ...e,
                image:e.image && e.image.url || null
            }
        });
      },
};
